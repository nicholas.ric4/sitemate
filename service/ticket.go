package service

import (
	"errors"
	"sitemate/entity"

	"github.com/go-playground/validator"
)

func RequestTickets() []entity.Ticket {
	// would hit database layer here
	return entity.Tickets
}

func CreateTicket(newTicket entity.CreateTicket) error {
	validate := validator.New()
	if err := validate.Struct(newTicket); err != nil {
		return err
	}

	// manual check for collisions in id due to no database. This could be skipped otherwise as duplicate would be found on creation
	for _, ticket := range entity.Tickets {
		if ticket.ID == newTicket.ID {
			return errors.New("post ticket: id collision")
		}
	}

	ticket := entity.Ticket{
		ID:          newTicket.ID,
		Title:       newTicket.Title,
		Description: newTicket.Description,
	}

	// would hit database layer here
	entity.Tickets = append(entity.Tickets, ticket)
	return nil
}

func UpdateTicket(id string, updatedTicket entity.UpdateTicket) error {
	validate := validator.New()
	if err := validate.Struct(updatedTicket); err != nil {
		return err
	}

	// would hit database layer here
	for i, _ := range entity.Tickets {
		if entity.Tickets[i].ID == id {
			entity.Tickets[i].Title = updatedTicket.Title
			entity.Tickets[i].Description = updatedTicket.Description
			return nil
		}
	}
	return errors.New("update ticket: ticket not found")
}

func DeleteTicket(id string) error {

	// would hit database layer here
	for i, _ := range entity.Tickets {
		if entity.Tickets[i].ID == id {
			entity.Tickets = append(entity.Tickets[:i], entity.Tickets[i+1:]...)
			return nil
		}
	}
	return errors.New("delete ticket: ticket not found")
}
