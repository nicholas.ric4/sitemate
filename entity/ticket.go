package entity

type Ticket struct {
	ID          string `json:"id" validate:"required"` // Uniqueness would be enforced by DB, here it will have to be through manual check
	Title       string `json:"title" validate:"required"`
	Description string `json:"description" validate:"required"`
}

type CreateTicket struct {
	ID          string `json:"id" validate:"required"` // Uniqueness would be enforced by DB, here it will have to be through manual check
	Title       string `json:"title" validate:"required"`
	Description string `json:"description" validate:"required"`
}

type UpdateTicket struct {
	Title       string `json:"title" validate:"required"`
	Description string `json:"description" validate:"required"`
}

// These tickets are used in place of a DB
var Tickets = []Ticket{
	{ID: "1", Title: "Make Breakfast", Description: "Make a piece of vegemite toast for breakfast"},
	{ID: "2", Title: "Morning Exercise", Description: "Do a 30-minute yoga session"},
	{ID: "3", Title: "Work on Project", Description: "Complete the draft of the project report"},
	{ID: "4", Title: "Lunch Preparation", Description: "Prepare a healthy salad for lunch"},
	{ID: "5", Title: "Read a Book", Description: "Read a chapter from 'To Kill a Mockingbird'"},
}
