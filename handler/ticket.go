package handler

import (
	"net/http"
	"sitemate/entity"
	"sitemate/service"

	"github.com/gin-gonic/gin"
)

func RequestTickets(c *gin.Context) {
	tickets := service.RequestTickets()
	c.IndentedJSON(http.StatusOK, tickets)
}

func PostTicket(c *gin.Context) {
	var newTicket entity.CreateTicket

	if err := c.BindJSON(&newTicket); err != nil {
		c.IndentedJSON(http.StatusBadRequest, err)
	}

	err := service.CreateTicket(newTicket)
	if err != nil {
		c.IndentedJSON(http.StatusBadRequest, err)
	}
	c.IndentedJSON(http.StatusOK, newTicket)
}

func PatchTicket(c *gin.Context) {
	id := c.Param("id")
	var updatedTicket entity.UpdateTicket

	if err := c.BindJSON(&updatedTicket); err != nil {
		c.IndentedJSON(http.StatusBadRequest, err)
	}

	err := service.UpdateTicket(id, updatedTicket)
	if err != nil {
		c.IndentedJSON(http.StatusBadRequest, err)
	}

	ticket := entity.Ticket{
		ID:          id,
		Title:       updatedTicket.Title,
		Description: updatedTicket.Description,
	}
	c.IndentedJSON(http.StatusOK, ticket)
}

func DeleteTicket(c *gin.Context) {
	id := c.Param("id")

	err := service.DeleteTicket(id)
	if err != nil {
		c.IndentedJSON(http.StatusBadRequest, err)
	}
	c.IndentedJSON(http.StatusOK, id)
}
