package main

import (
	"sitemate/handler"

	"github.com/gin-gonic/gin"
)

func main() {
	router := gin.Default()
	router.GET("/tickets", handler.RequestTickets)
	router.POST("/ticket", handler.PostTicket)
	router.PATCH("/ticket/:id", handler.PatchTicket)
	router.DELETE("/ticket/:id", handler.DeleteTicket)

	router.Run("localhost:8080")
}
